# -*- coding: utf-8 -*-
#
# Copyright (C) 2016 Alexandar P. Mechev
# All rights reserved.
#
# This software is licensed as described in the file LICENSE.md, which
# you should have received as part of this distribution.

"""
.. module:: GRID_LRT.safepopen
   :platform: Unix
   :synopsis: Wrapper around subprocess.Popen to deal with Python 2 and Python 3 differences

.. moduleauthor:: Alexandar Mechev <LOFAR@apmechev.com>
"""

import GRID_LRT
import sys

from subprocess import Popen

__version__ = GRID_LRT.__version__
__author__ = GRID_LRT.__author__
__license__ = GRID_LRT.__license__
__email__ = GRID_LRT.__email__
__copyright__ = GRID_LRT.__copyright__
__credits__ = GRID_LRT.__credits__
__maintainer__ = GRID_LRT.__maintainer__
__status__ = GRID_LRT.__status__

class SafePopen(Popen):
    def __init__(self, *args, **kwargs):
        if sys.version_info.major == 3 : 
            kwargs['encoding'] = 'utf8'
        return super(SafePopen, self).__init__(*args, **kwargs)

